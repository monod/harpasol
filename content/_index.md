---
title: "HarpaSol"
date: 2019-09-04T14:34:05-05:00
draft: false
---
**Ha**rdware **pa**ra **So**ftware **l**ibre

* *No es solo [Hardware Libre][hl]*: sería nuestro ideal.
* *No es solo [Hardware certificado por la FSF][hcf]*: en ocaciones tenemos modelos que si.
* *No es solo [hardware compatible con software 100% libre][hcsl]*: aspiramos a ir avanzando en ese camino.

**Harpasol** tiene el proposito de proveer [hardware][p] y [soporte][s] lo más compatible posible con el [movimiento del Software Libre][msl] en el contexto colombiano, partimos de la premisa que aunque el ideal sería el uso de *Software Libre en una Sociedad Libre*, tenemos que plantearnos el uso del *Software Libre en una Sociedad no Libre* y más puntualmente en un *Mercado de Hardware no Libre*; pero no quiere decir que no nos importa que *el hardware que usamos no sea hardware libre y requiera software privativo para funcionar* pues si esto no nos importara no existiría **HarpaSol**, bastaría con las tiendas de hardware actuales de nuestro mercado.

1. En principio [migrabamos a software libre][misl] y usabamos [controladores privativos][cp].
2. Como los [controladores privativos][cp] son un problema para el [Software Libre][0] procurabamos [asesorar antes de la compra de equipos][ah] para adquirir equipos menos restrictivos con el [Software Libre][0].
3. Luego formó parte de nuestras Herramientas ya no solo las memorias vivas y copias (mirrors) de [distribuciones de Software Libre][dsl] sino dispositivos (como tarjetas inalámbricas) para reemplazar los dispositivos incompatibles con [Software Libre][0].
4. Luego logramos avanzar y migrar no solo el [Sistema Operativo][so] sino la [Bios][b] de algunos equipos permitiendo llevar software libre a otros espacios y además saltar restricciones de reemplazo de Hardware ([como las tarjetas inalámbricas en portátiles][ip]) por hardware compatible con [Software Libre][0].
5. **Hoy Aspirmos poner a disposición el hardware más cercano al [movimiento del Software Libre][msl] que nos es accequible en nuestro contexto económico y social.**

[0]: /conceptos/software_libre
[hl]: /conceptos/hardware_libre
[hcf]: /conceptos/fsf_ryf_certificate
[hcsl]: /conceptos/hardware_compatible_con_software_100_libre
[p]: /products
[s]: /soporte
[msl]: /conceptos/movimiento_software_libre
[misl]: /conceptos/migracion_a_software_libre
[cp]: /conceptos/controladores_privativos
[ah]: /conceptos/asesoria_hardware
[dsl]: /conceptos/distribuciones_de_software_libre
[so]: /conceptos/sistema_operativo
[b]: /conceptos/bios
[ip]: /conceptos/restriccion_inalambricas_en_bios
