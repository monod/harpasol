+++
title = "Guix"
date = 2019-09-04T08:50:05-05:00
draft = false
tags = ["distribuciones", "guix", "gnu"]
+++
The *[GNU Guix][guix]* package and system manager is a [free software][sl] project developed by volunteers around the world under the umbrella of the [GNU Project][gp]. 

Guix System is an advanced distribution of the [GNU operating system][gnu]. It uses the [Linux-libre][ll] kernel, and support for [the Hurd][h] is being worked on. As a GNU distribution, it is committed to respecting and enhancing [the freedom of its users][sl]. As such, it adheres to the [GNU Free System Distribution Guidelines][fd].

GNU Guix provides [state-of-the-art package management features][f] such as transactional upgrades and roll-backs, reproducible build environments, unprivileged package management, and per-user profiles. It uses low-level mechanisms from the [Nix][n] package manager, but packages are [defined][def] as native [Guile][g] modules, using extensions to the [Scheme][s] language—which makes it nicely hackable.

Guix takes that a step further by additionally supporting stateless, reproducible [operating system configurations][osc]. This time the whole system is hackable in Scheme, from the [initial RAM disk][ird] to the [initialization system][is], and to the [system services][ss].

[guix]: https://guix.gnu.org/
[gnu]: http://www.gnu.org/
[sl]: https://gnu.org/philosophy/free-sw.html
[gp]: https://gnu.org/
[ll]: https://gnu.org/software/linux-libre
[h]: https://gnu.org/software/hurd
[fd]: https://gnu.org/distros/free-system-distribution-guidelines.html
[f]: https://guix.gnu.org/manual/en/html_node/Features.html
[n]: https://nixos.org/nix/
[def]: https://guix.gnu.org/manual/en/html_node/Defining-Packages.html
[g]: https://gnu.org/software/guile
[s]: http://schemers.org/
[osc]: https://guix.gnu.org/manual/en/html_node/Using-the-Configuration-System.html
[ird]: https://guix.gnu.org/manual/en/html_node/Initial-RAM-Disk.html
[is]: https://gnu.org/software/shepherd
[ss]: https://guix.gnu.org/manual/en/html_node/Defining-Services.html




