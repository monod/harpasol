+++
title = "Parabola GNU/Linux-libre"
date = 2019-09-04T08:50:05-05:00
draft = false
tags = ["distribuciones", "parabola", "arch", "gnu"]
+++
[Parabola][p] es un proyecto de [software libre][sl] y [cultura libre][cl] que intenta proveer una distribución GNU/Linux completamente libre llamada Parabola GNU/Linux-libre. Está basada en en los paquetes de [Arch (la distribución GNU/Linux)][a] y posiblemente otros sistemas basados en Arch, con paquetes optimizados para arquitecturas i686, x86_64 y armv7h. Parabola pretende que sus herramientas de administración y sus paquetes sean simples. El objetivo es brindar al usuario control total de su sistema con un 100% de software libre y cultura libre. **Parabola GNU/Linux-libre** está listada por la [Free Software Foundation][fsf] como un sistema operativo totalmente libre.

El desarrollo se enfoca en la simplicidad, elegancia, claridad del código y ofrecer lo último en software libre. Su diseño ligero y simple lo hace fácil de extender a cualquier otro tipo de sistema que quieras construir.

Puedes encontrarnos en [#parabola][#p] o en nuestra lista de correos: [mailing list][ml]. 



[p]: https://www.parabola.nu/
[sl]: https://www.gnu.org/philosophy/free-sw.es.html
[cl]: http://freedomdefined.org/Definition/Es
[a]: https://www.archlinux.org/
[fsf]: https://www.fsf.org/ 
[#p]: irc://irc.freenode.net/#parabola
[ml]: https://lists.parabola.nu/mailman/listinfo

